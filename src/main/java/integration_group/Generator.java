package integration_group;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse erstellt aus einer Klasse ein Interface. Zuerst wird eine
 * ArrayList erzeugt und anschliessend in ein File mit dem Namen der Klasse und
 * eine "If" davor erstellt. Das Interface ist kompilierbar und kann
 * anschliessend verwendet werden.
 *
 * @author Yvo Broennimann, Giancarlo Bergamin
 *
 */
public class Generator {

	List<String> lines;

	/**
	 * Im Konstruktor wird eine ArrayList erzeugt, die anschliessend
	 * Ausgangspunkt für die Interfaceerzeugung ist. Alle Zeilen des Interfaces
	 * werden zuerst in die ArrayList geschrieben
	 */
	public Generator() {
		lines = new ArrayList<>();
	}

	/**
	 * Generiert ein semantisch korrektes Interface mit allen Methoden der
	 * mitgegeben Klasse sowie deren Superklassen mit Ausnahme der Methode der
	 * Klasse Object
	 *
	 * @param c
	 *            Klasse für die das Interface erstellt werden soll.
	 */
	public void generateInterface(Class<?> c) {
		lines.add("public interface If" + c.getSimpleName() + "{");
		Method[] methods = c.getMethods();
		for (Method method : methods) {
			if (method.getDeclaringClass().getName() != "java.lang.Object") {
				organizeImportsForParams(method.getParameterTypes());
				organizeImports(method.getReturnType());
				if (!Modifier.isStatic(method.getModifiers()) & !Modifier.isFinal(method.getModifiers())) {
					lines.add(method.getReturnType().getSimpleName() + " " + method.getName() + "("
							+ prepareParameterTypeString(method) + ");");
				}
			}
		}
		lines.add("}");
		saveToFile(lines, c);
	}

	/**
	 * Ueberprueft für jede Methode, ob für die Parameter Klassen noch imports
	 * gemacht werden muessen. Es werden nur Klassen importiert, die nicht im
	 * Package java.lang sind.
	 *
	 * @param method
	 */
	private void organizeImportsForParams(Class<?>[] paramTypes) {
		for (Class<?> paramType : paramTypes) {
			organizeImports(paramType);
		}
	}

	/**
	 * Ueberprueft für jede Methode, ob für die angegeben Klasse noch imports
	 * gemacht werden muessen. Es werden nur Klassen importiert, die nicht im
	 * Package java.lang sind und nicht schon eine Import Anweisung haben
	 *
	 * @param method
	 */
	private void organizeImports(Class<?> classToCheck) {
		if (!classToCheck.getName().matches(".*java.lang.*")
				&& !lines.contains("import " + classToCheck.getTypeName().replaceAll("\\[\\]", "") + ";") //Schaut ob der Import schon gemacht wurde
				&& !classToCheck.isPrimitive()
				) {
			lines.add(0, "import " + classToCheck.getTypeName().replaceAll("\\[\\]", "") + ";"); // Schreibt die Importanweisung an den Anfang, das Replace loescht eventuelle Array Klammern
		}
	}

	/**
	 * Methode die alle Parametertypen im richtigen Format als String zurueck
	 * gibt, der mit Komma die einzelnen Parameter trennt.
	 *
	 * @param method
	 * @return
	 */
	private String prepareParameterTypeString(Method method) {
		Class<?>[] params = method.getParameterTypes();
		ArrayList<String> paramList = new ArrayList<>();
		int index = 0;
		for (Class<?> param : params) {
			paramList.add(param.getSimpleName() + " e" + index++);
		}
		return String.join(", ", paramList);
	}

	/**
	 * Speichert die List in ein File names IfKlassenname>.java
	 *
	 * @param l
	 *            List aus der das File generiert werden soll
	 * @param c
	 *            Klasse aus der die List erstellt wurde, um das File richtig zu
	 *            benennen
	 */
	private void saveToFile(List<String> l, Class<?> c) {
		Path file = Paths.get("If" + c.getSimpleName() + ".java");
		try {
			Files.write(file, l);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
