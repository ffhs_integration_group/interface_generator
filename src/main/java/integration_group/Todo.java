package integration_group;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Meta annotaions
 * @author giancarlo, yvo
 *
 */
@Retention(RetentionPolicy.RUNTIME) //Die annotation ist auch noch während der Laufzeit verfügbar
@Target({ElementType.METHOD, ElementType.TYPE})  //Die annotation kann nur auf Klassen, Interfaces, Enums, Annotations und Methoden angewendet werden


/**
 * Eine Annotation, welche zwei Werte besitzt: what des Types String und when des Types int.
 * 
 * @author giancarlo, yvo
 *
 */
public @interface Todo {

	String what();
	int when();
	
		
}
