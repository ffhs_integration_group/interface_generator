package integration_group;

public class Main {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: Main <Classname>");
			System.exit(0);
		}
		Class<?> c = classIsReachable(args[0]);
		if (c != null) {
			Generator g = new Generator();
			g.generateInterface(c);
			System.out.println("Interface generated in the root directory of the project!");
		} else {
			System.out.println("Please enter a valid classname or specify the right classpath");
		}
	}

	private static Class<?> classIsReachable(String classname) {
		Class<?> c;
		try {
			c = Class.forName(classname);
		} catch (ClassNotFoundException e) {
			c = null;
		}
		return c;
	}

}
