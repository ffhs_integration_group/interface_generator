/**
 *
 */
package integration_group;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yvo Broennimann, Giancarlo Bergamin
 *
 */
public class GeneratorTest {

	/**
	 * Testet Methode GenerateInterface indem es das generierte File mit einem Referenzfile im Ordner java/test vergleicht.
	 */
	@Test
	public void testGenerateInterface() {
		Generator g = new Generator();
		ArrayList<String> e = new ArrayList<>();
		g.generateInterface(e.getClass());
		try {
			String[] str = readLines("IfArrayList.java");
			String[] reference = readLines("src/test/java/IfArrayList.java");
			Assert.assertArrayEquals( str, reference );
		}
		catch(IOException e1){
			Assert.fail();
		}

	}
	/**
	 * Hilfsmethode um aus einem Text oder hier java File alle Zeilen auszulesen und als Array zurueck zu geben.
	 * Es ist nicht moeglich die Files direkt korrekt zu vergleichen, mithilfe eines Asserts.
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public String[] readLines(String filename) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
        List<String> lines = new ArrayList<String>();
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        bufferedReader.close();
        String[] lineArray = new String[lines.size()];
        return lines.toArray(lineArray);
    }

}
